import { AdpTzWidgetPage } from './app.po';

describe('adp-tz-widget App', function() {
  let page: AdpTzWidgetPage;

  beforeEach(() => {
    page = new AdpTzWidgetPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
