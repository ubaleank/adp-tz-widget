import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Time Zone Assistant';
  subtitle = 'Helps to schedule convenient meeting time';
  showAddContainer = false;

  onAdd(){
      this.showAddContainer = true;
  }

  onClose(close:boolean){
    this.showAddContainer = close;
  }
}
